var translations =
   {
      "et": [
         {
            "first_question"  :  "Tervise<br>infosüsteem<br>Digilugu",
            "second_question" :  "E-tervise<br>patsiendi-<br>portaal",
            "third_question"  :  "Digiretsept",
            "watch_video"     :  "Vaata",
            "video_one"       :  "videos/sample_video.mp4",
            "video_two"       :  "videos/sample_video_2.mp4",
         }
      ],
      "en": [
         {
            "first_question"  :  "Digilugu<br>Health<br>Information System ",
            "second_question" :  "e-Health<br>Patient<br>Portal",
            "third_question"  :  "Digital Prescription",
            "watch_video"     :  "Play",
            "video_one"       :  "videos/sample_video.mp4",
            "video_two"       :  "videos/sample_video_2.mp4",
         }
      ],
      "ru": [
         {
            "first_question"  :  "Инфосистема<br>здоровья<br>Digilugu",
            "second_question" :  "портал<br>э-здоровья<br>для пациента ",
            "third_question"  :  "цифровой рецепт",
            "watch_video"     :  "Смотреть",
            "video_one"       :  "videos/sample_video.mp4",
            "video_two"       :  "videos/sample_video_2.mp4",
         }
      ],
      "fi": [
         {
            "first_question"  :  "Terveystietojärjestelmä<br>Digilugu",
            "second_question" :  "E-terveyspalvelun<br>potilasportaali",
            "third_question"  :  "Sähköinen resepti",
            "watch_video"     :  "Katso",
            "video_one"       :  "videos/sample_video.mp4",
            "video_two"       :  "videos/sample_video_2.mp4",
         }
      ]
   }